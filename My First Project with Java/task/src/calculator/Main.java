package calculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int earnedAmountBubblegum = 202,
                earnedAmountToffee = 118,
                earnedAmountIceCream = 2250,
                earnedAmountMilkChocolate = 1680,
                earnedAmountDoughnut = 1075,
                earnedAmountPancake = 80;
        System.out.println("Earned amount:");
        System.out.printf("Bubblegum: $%d \n", earnedAmountBubblegum);
        System.out.printf("Toffee: $%d \n", earnedAmountToffee);
        System.out.printf("Ice cream: $%d \n", earnedAmountIceCream);
        System.out.printf("Milk chocolate: $%d \n", earnedAmountMilkChocolate);
        System.out.printf("Doughnut: $%d \n", earnedAmountDoughnut);
        System.out.printf("Pancake: $%d \n\n", earnedAmountPancake);
        int sum = earnedAmountBubblegum + earnedAmountToffee + earnedAmountIceCream
                + earnedAmountMilkChocolate + earnedAmountDoughnut + earnedAmountPancake;
        System.out.printf("Income: $%d \n", sum);

        System.out.println("Staff expenses:");
        int expenses = scanner.nextInt();

        System.out.println("Other expenses:");
        int otherExpenses = scanner.nextInt();

        int netIncome = sum - (expenses + otherExpenses);

        System.out.printf("Net income: $%d", netIncome);
    }

}